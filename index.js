#! /usr/bin/env node

'use strict';

require('dotenv').config();

const cheerio = require('cheerio');

const RETRY_SECONDS = process.env.RETRY_SECONDS || 45;
const MAX_COLOURS = 6;

/**
 *  Check debugging flag.
 *
 *  Example usage: debugging() && console.debug('011: Here's some info!');
 *
 *  @function debugging
 *  @param (string?)  tag   If present, will be matched against tokens from
 *    env.DEBUG
 */
const debugging = process.env.DEBUG
  ? (function $$debugging(tag) {
    // unconditional debug? Quick yes
    if (!tag) return true;

    const tags = process.env.DEBUG.split(/\s*,\s*/);

    return tags.includes(tag) || tags.includes('all') || tags.includes('*');
  })
  : (()=>{});


// Masto streaming API
const WebSocket = require('ws');

const access_token = process.env.MASTOBOT_TOKEN;
const tag = 'CheerLights';

// console.debug('038:', access_token);

// Mastobot API to interact back to Mastodon
const { MastoBot } = require('mastobot');
const api_domain = 'botsin.space';
const api_url    = `https://${api_domain}/`;


const mb = new MastoBot(api_url, {
  api_token: access_token,
});


const stream = { 
  user: false,
  hashtag: false,
};


// === the actual bot: start the streams ===

startUserStream();
startHashtagStream();
Promise.all([
  new Promise(resolve => stream.user.once('open', resolve)),
  new Promise(resolve => stream.hashtag.once('open', resolve)),
])
.then(status => {
  console.info(`066: Connected initial User and Hashtag streams`);
})
.catch(error => {
  console.warn(`069: Failed to connect initial Hashtag and User streams`);
});



// *******************************
// *****************************
// ***                   *****
// *** support functions ***
// ***                   *****
// *****************************
// *******************************

function startUserStream()
{
  if (stream.user)
  {
    debugging('stream') && console.info(`094: Closing old user stream...`);
    try
    {
      stream.user.removeAllListeners();
      stream.user.close();
    }
    catch (error)
    {
      console.warn('107: Error closing old user stream:', error);
    }
  }
  debugging('stream') && console.debug(`105: Opening new user stream ...`);
  stream.user = connectStream(`wss://${api_domain}/api/v1/streaming?access_token=${access_token}&stream=user`, onMastoEvent);

  stream.user.on('close', () => {
    console.debug('103: user stream closed; reconnecting soon...');
    setTimeout(() => {
      console.log('105: reconnecting user stream now');
      startUserStream();
    }, RETRY_SECONDS * 1000);
  });
}

function startHashtagStream()
{
  if (stream.hashtag)
  {
    debugging('stream') && console.info(`115: Closing old hashtag stream...`);
    try
    {
      stream.hashtag.removeAllListeners();
      stream.hashtag.close();
    }
    catch (error)
    {
      console.warn('123: Error closing old hashtag stream:', error);
    }
  }
  debugging('stream') && console.debug(`126: Opening new hashtag stream...`);
  stream.hashtag = connectStream(`wss://${api_domain}/api/v1/streaming?access_token=${access_token}&stream=hashtag&tag=${tag}`, onMastoEvent);

  stream.hashtag.on('close', () => {
    console.debug('128: hashtag stream closed. Reopening soon...');
    setTimeout(() => {
      console.debug('130: reconnecting hashtag stream now');
      startHashtagStream();
    }, RETRY_SECONDS * 1000);
  });
}


const colourRE = /\b((red|green|blue|cyan|white|oldlace|purple|magenta|yellow|orange|pink)|#?(FF0000|008000|0000FF|00FFFF|FFFFFF|FDF5E6|800080|FF00FF|FFFF00|FFA500|FFC0CB))\b/ig;
const hexcodes = {
  "FF0000": "red",
  "008000": "green",
  "0000FF": "blue",
  "00FFFF": "cyan",
  "FFFFFF": "white",
  "FDF5E6": "oldlace",
  "800080": "purple",
  "FF00FF": "magenta",
  "FFFF00": "yellow",
  "FFA500": "orange",
  "FFC0CB": "pink",
};

const MAX_CACHE_SIZE = 100;
const statusIdCache = [];


function connectStream(wsUrl, onMastoEvent)
{
  const client = new WebSocket(wsUrl);
  const logUrl = String(wsUrl)
    .replace(/^[^?]*/, '')
    .replace(/access_token=[^&]*/, '');
  if (!isFinite(connectStream.idx))
    connectStream.idx = 0;
  const idx = connectStream.idx++;
  let broken = 0;

  client.on('open', () => {
    debugging('stream') && console.log(`157: Connected websocket #${idx}`, logUrl);
  });

  client.on('close', (code, reason) => {
    console.log(`161: closed websocket #${idx}`, logUrl, { code, reason: reason?.toString('utf8') });
    // additional handler is attached in startXXXStream to reopen conn
  });

  client.on('error', error => {
    const [, errorCode] = (error.message?.match(/Unexpected server response: (\d+)/)) || [];
    if (errorCode && !error.code)
      error.code = parseInt(errorCode, 10);

    if (error.code === 401)
      console.warn(`194: 401 Bad Authorization from websocket #${idx} ${logUrl}.`, error.message);
    else if (error.code === 502)
      console.warn(`196: 502 Bad Gateway error from websocket #${idx} ${logUrl}.`, error.message);
    else if (error.code)
      console.warn(`198: Error ${error.code} from websocket #${idx} ${logUrl}.`, error.message);
    else
      console.warn(`168: Unexpected error from websocket #${idx}`, logUrl, error);

/*    if (broken)
    {
      console.log(`171: Consecutive unexpected errors, recycle the connection in ${broken * 3}s...`);
      setTimeout(() => {
        try
        {
          debugging('stream') && console.log(`200: Closing websocket #${idx}...`);
          client.close();
        }
        catch (error)
        {
          console.warn(`178: Error closing broken websocket #${idx}`);
        }
      }, broken * 3 * 1000);
    }
    broken++; // */
  });

  client.on('message', (data, isBoolean) => {
    const msg = data.toString('utf8');
    const msgOb = JSON.parse(msg);

    const stream = msgOb.stream;
    const event = msgOb.event;
    const payload = JSON.parse(msgOb.payload);

    // clear the "broken" flag for this socket
    broken = 0;

    return onMastoEvent(stream, event, payload);
  });

  return client;
}



// === Handlers for stuff from Masto ===


// unwrap a raw event from a Mastodon stream, poass it on to next-level handler
function onMastoEvent(stream, event, payload)
{
  debugging('events') && console.debug('058: onMastoEvent', stream, event, payload.id, 0&& payload && Object.keys(payload));
  switch (event) {
    case 'notification':
      return onNotification(stream, event, payload);
    case 'update':
    case 'status.update':
      return handleStatus(payload);

    // Known, but otherwise ignorable, eevnts:
    case 'announcement.reaction':
    case 'delete':
      return;

    default:
      console.warn('! unexpected message.event', stream, event, payload);
  }
}


// handle a Notification event from Masto
function onNotification(stream, event, payload)
{
  debugging('notifications') && console.debug('071: onNotification', stream, event, payload.id, 0&& payload && Object.keys(payload));

  if (payload.account.acct === 'cymplerobot')
    mb.dismissNotification(payload.id)
    .catch(error => {
      console.warn(`267: Failed to dismiss notification from CympleCyRobot`, error.message);
    });

  const { type } = payload;
  switch (type) {
    case 'mention':
      return handleStatus(payload.status);

    case 'follow':
      return handleFollow(payload);

    case 'favourite':
      return handleFave(payload);

    case 'reblog':
      return handleReblog(payload);

    default:
      console.warn('! unexpected notification', stream, type, payload);
  }
}


// Handle a fave
async function handleFave({account, status})
{
  console.log('! new favourite:', account.acct, await extractText(status.content));
}


// Handle a boost
async function handleReblog({account, status})
{
  console.log('! new boost:', account.acct, await extractText(status.content));
}


// Handle a follow
async function handleFollow({account})
{
  console.log('! new follower:', account.acct);

  followUser(account.id);
}


// Handle an incoming Toot
async function handleStatus(status) 
{
  debugging('status') && console.debug('086: handleStatus', status?.id);

  const { id, created_at, spoiler_text, visibility, url, content, account } = status;
  const { display_name, acct } = account;

  // convert hex values to html colour names
  const text = await extractText(content);
  

  // on duplicated status (eg. user feed and hashtag feed), bail
  if (statusIdCache.includes(id))
  {
    debugging('status') && console.debug(`094: already handled status ${id}`);
    return;
  }

  // only process toots that @ or # CheerLights and/or testTagNNN
  if (!text.match(/[@#]CheerLights|testTag\d+/i))
    return;

  // if we are processing the status, add it to the cache, pruning oldest
  // entries if needed
  statusIdCache.push(id);
  if (statusIdCache.length > MAX_CACHE_SIZE)
    statusIdCache.splice(0, statusIdCache.length - MAX_CACHE_SIZE);


  // maybe just for debug, log the toot
  //console.log('! status:', stream, event, type);
  if (debugging('status'))
  {
    console.log(`${visibility} from: ${display_name} @${acct}`);
    console.log('  Date: ' + created_at );
    console.log('  ' + text);
  }

  // Look for a slash-command or "help", call into interaction system if found,
  // early-out without looking for colours if we did something useful
  if (text.includes('help') || text.match(/\/\w+/))
  {
    const handled = await handleSlashCommand(status, text);
    if (handled !== false)
      return;
  }


  // look for a colour, pass the message on to CheerLights if found
  if (colourRE.test(text))
  {
    return handleColourStatus(status, text)
      .catch(error => {
        console.error('222: Unexpected error posting tweet:', error);
      });
  }
}


/** Process toot content to remove any HTML, etc, and process hex colour codes */
async function extractText(content)
{
  return (await cheerio.load(content)).text().replace(/#[0-9a-f]{6}/gi, orig => {
    const hex = orig.toUpperCase().replace(/^#/, '');
    return hexcodes[hex] || orig;
  });
}


/** Handle a Toot containing a colour, extracting/processing the text if needed */
async function handleColourStatus(status, text = '')
{
  const { visibility, content, account } = status;
  const { display_name, acct } = account;

  if (!text)
    text = await extractText(content);

  const colours = text.match(colourRE);

  // Fucking spammers
  if (colours.length > MAX_COLOURS)
    colours.length = MAX_COLOURS;

  debugging('status') && console.log('Colour detected!', colours);

  const webhookPayload = {
    source: 'mastodon',
    colours,
    username: acct,
    text,
  };

  const authn = Buffer.from(`${process.env.WEBHOOK_USERNAME}:${process.env.WEBHOOK_PASSWORD}`).toString('base64');

  console.log('403:', acct, colours);

  const sent = await mb._request('post', process.env.WEBHOOK_URL, {
    body: JSON.stringify(webhookPayload),
    headers: {
      Authorization: `Basic ${authn}`,
      'Content-type': 'application/json',
    },
  });

  debugging('webhook') && console.log('sent webhook; response=', sent.statusCode, sent.statusText, sent.data);
}


async function handleSlashCommand(status, text = '')
{
  if (!text)
    text = await extractText(status.content);

  const [command] = text.match(/[/]\w+|[/]?help/);
  const userId = status.account.id;

  if (command)
    console.log(`! Got command ${command} from ${status.account.acct}`);

  switch (command.toLowerCase())
  {
    default:      // if no recognized command, then 
      return false;

    case 'help':
    case '/help':
      return sendHelp(status)
        // .then(() => mb.favouriteStatus(status.id))
        .catch(mbErrorCatcherFactory('help'));

    case '/follow':
      return followUser(userId)
        .then(() => mb.favouriteStatus(status.id))
        .catch(mbErrorCatcherFactory('follow'));
    case '/unfollow':
      return unfollowUser(userId)
        .then(() => mb.favouriteStatus(status.id))
        .catch(mbErrorCatcherFactory('unfollow'));

    // case '/blockme':
    //   return blockUser(status);
  }

}


// Returns a generic error handler which reports then swallows the error
function mbErrorCatcherFactory(mbAction)
{
  return (error) => {
    console.warn(`397: Unexpected Mastobot error during ${mbAction}:`, error);
  }
}

async function sendHelp(status)
{

}

async function followUser(userId)
{
  console.log(`- Following user ${userId}`);
  return mb.followAccount(userId);
}

async function unfollowUser(userId)
{
  console.log(`- Unfollowing user ${userId}`);
  return mb.unfollowAccount(userId);
}

// async function blockUser(status)
// {

// }
